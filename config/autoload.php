<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 18:37
 */

/**
 * -------------------------------------------------------------------------
 * CLASSES
 * -------------------------------------------------------------------------
 */
ClassLoader::addClasses([
    'Hypemedia\\Contao\\PageBoxes\\Inserttag\\Pageboxes' => 'system/modules/hype-contao-boxes/src/Hypemedia/Contao/PageBoxes/Inserttag/Pageboxes.php',
    'Hypemedia\\Contao\\PageBoxes\\DCA\\TlPage' => 'system/modules/hype-contao-boxes/src/Hypemedia/Contao/PageBoxes/DCA/TlPage.php',
    'Hypemedia\\Contao\\PageBoxes\\Module\\Pageboxes' => 'system/modules/hype-contao-boxes/src/Hypemedia/Contao/PageBoxes/Module/Pageboxes.php'
]);

/**
 * -------------------------------------------------------------------------
 * TEMPLATES
 * -------------------------------------------------------------------------
 */
TemplateLoader::addFiles([
    'mod_hpb_default' => 'system/modules/hype-contao-boxes/templates',
    'hpb_box_default' => 'system/modules/hype-contao-boxes/templates'
]);