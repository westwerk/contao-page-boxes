<?php

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_legend'] = 'Box-Ansicht';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_title'][0] = 'Box-Titel';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_title'][1] = 'Legen Sie den Titel für die Box-Ansicht fest.';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_subtitle'][0] = 'Box-Untertitel';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_subtitle'][1] = 'Legen Sie den Untertitel für die Box-Ansicht fest.';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_description'][0] = 'Box-Beschreibung';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_description'][1] = 'Legen Sie die Beschreibung für die Box-Ansicht fest.';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_image'][0] = 'Box-Bild';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_image'][1] = 'Legen Sie das Vorschaubild für die Box-Ansicht fest.';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_enable_page'][0] = 'Box-Ansicht aktivieren';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_enable_page'][1] = 'Ermöglicht, diese Seite als Box anzuzeigen.';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_selection'][0] = 'Box-Auswahl';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_selection'][1] = 'Seiten mit Box-Ansicht zur Anzeige auswählen.';